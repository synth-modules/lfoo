// Hardware pin configuration
const byte lfo_pins[] = { 3, 5, 6, 9, 10, 11 };
const byte pot_pins[] = { A0, A1, A2 };
const byte trig_pin = 2;

// Init setup variables
const int fadeAmount = 1;
const int lfo_peak = 250;
const int max_period = 400;

// Vars which allocate memory
// to run the program
byte brightness_[] = { 0, 0, 0 };
byte sign[] = { 1, 1, 1 };
int val[3];
int time_delay = 0;
boolean trig_state = false;

// Declare lfoo
// function just
// before setup()
void lfoo(void);

void setup() {
  for (int i = 0; i < sizeof(lfo_pins); i++)
    pinMode(lfo_pins[i], OUTPUT);

  pinMode(trig_pin, INPUT);
}

void loop() {
  for (int i = 0; i < 3; i++) lfoo(i);
  
  time_delay++;
  if(time_delay > max_period) time_delay = 0;
}

void lfoo(int i) {
  // Update the pot value
  val[i] = map(analogRead(pot_pins[i]), 0, 1023, 1, max_period);

  // Check if it's time to update 
  if (time_delay % val[i] == 0)
  {
    // Update the pwm value
    analogWrite(lfo_pins[i], brightness_[i]);
    analogWrite(lfo_pins[i + 3], abs(brightness_[i] - lfo_peak));

    // Check brightness's borders
    brightness_[i] += (fadeAmount * sign[i]);
    if (brightness_[i] <= 0 || brightness_[i] >= lfo_peak) sign[i] *= -1;
  }

  // If the button is pressed, all the brightness values
  // are reset. Press the button again to reset again, etc.
  if (digitalRead(trig_pin) == HIGH && trig_state == false)
  {
    for (int j = 0; j < sizeof(brightness_); j++)
      brightness_[j] = 0;
    trig_state == true;
  } else if (digitalRead(trig_pin) == LOW)
    trig_state == false;
}
